#pragma once

#include <string>
#include <iostream>

using namespace std;

namespace Mert
{
	class LPS
	{
	public:
		LPS();
		~LPS();

		static string longestPalindrome(string s);
		static bool isPalindrome(string s);

	private:

	};

	LPS::LPS()
	{
	}

	LPS::~LPS()
	{
	}

	inline string LPS::longestPalindrome(string s)
	{
		string longestPalin = s.substr(0, 1);
		int maxPalinVal = 1;
		for (int i = 0; i < s.length(); i++) {

			int index = i;
			char ch = s[index];
			while ((index = s.find(ch, index + 1)) != -1) {

				string substr = s.substr(i, index + 1 - i);
				if (isPalindrome(substr)) {
					if ((index - i + 1) > maxPalinVal) {
						maxPalinVal = index - i + 1;
						longestPalin = s.substr(i, index + 1 - i);
					}
				}
			}
		}
		return longestPalin;


	}

	inline bool LPS::isPalindrome(string s)
	{
		if (s == string(s.rbegin(), s.rend()))
			return true;
		return false;
	}
}
