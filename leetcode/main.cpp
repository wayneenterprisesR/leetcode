// main.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int main() {

	// Q#5: Longest Palindromic Substring
	printf("Execution started for Mert::LPS::longestPalindrome\n");
	auto begin = std::chrono::high_resolution_clock::now();

	string exampleString = "azyebmucooooooooooocumbeyza";

	printf("%s\n", Mert::LPS::longestPalindrome(exampleString).c_str());

	auto end = std::chrono::high_resolution_clock::now();

	printf("Execution time is %.9f seconds\n", std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() / 1000000000.0);
	//=============================================================================

	cin.get();
	cin.ignore();
	return 0;
}